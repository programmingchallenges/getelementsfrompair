import unittest
import main

class TestStringMethods(unittest.TestCase):

    def testBasic(self):
        self.assertEqual(main.car(main.cons(3, 4)), 3)

    def testRootOnly(self):
        self.assertEqual(main.cdr(main.cons(3, 4)), 4)

if __name__ == '__main__':
    unittest.main()