import sys  

def cons(a, b):
    def pair(f):
        return f(a, b)
    return pair

def car(pair):
    return pair(getFirstElement)

def cdr(pair):
    return pair(getLastElement)

def getFirstElement(a, b):
    return a

def getLastElement(a, b):
    return b

if __name__ == '__main__':
    print('No main for this file')
